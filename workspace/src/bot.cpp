#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>


int main()

{
	//1. Declarar una variable puntero a DatosMemCompartifa
	DatosMemCompartida* puntero;
	
	//Se abre en el descriptor de ficheros fd el archivo proyectado sacado de compilar anteriormente tenis.cppp
	int fd;
	struct stat info;//para obtener el tamaño de fd
	void* tmp;
	fd=open("/tmp/memoria.dat", O_RDWR, 0666); //se crea en modo lectura y escritura el fichero memoria.dat
	if (fd == -1)
		perror("Error al abrir archivo memoria.dat en bot.cpp");

	fstat(fd,&info);//se guarda en info la informacion de fd
	tmp = mmap(NULL,info.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0); //tmp seria el archivo en el que se proyecta la memoria
	close(fd);//se cierra el descriptor de fichero, pues se pretende trabajar en memoria
	if(tmp==(void*)-1)
		perror("Error en bot.cpp al proyectar en memoria");
	//se asigna a la direccion de memoria 
	else puntero=(DatosMemCompartida*)tmp;
	
	
	
	while(1)
	{
		//algoritmo de movimientos RAQUETA 1 
		if(puntero->esfera.centro.y < (puntero->raqueta1.y1+puntero->raqueta1.y2)/2)
			puntero->accion = -1;
		else if(puntero->esfera.centro.y > (puntero->raqueta1.y1+puntero->raqueta1.y2)/2)
			puntero->accion = 1;
		else puntero->accion = 0;
		

		//algotitmo movimientos raqueta 2, solo si el tiempo supera los 10 segundos
		if(puntero->tiempo >= 10.0)
		{
			if(puntero->esfera.centro.y < (puntero->raqueta2.y1+puntero->raqueta2.y2)/2)
				puntero->accion2 = -1;
			else if(puntero->esfera.centro.y > (puntero->raqueta2.y1+puntero->raqueta2.y2)/2)
				puntero->accion2 = 1;
			else puntero->accion2 = 0;
		}
		usleep(25000);
	}
}
